package ru.vyatsu.whitebox;

import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayUtilTest {

    @Test
    public void testEmptyArray() {
        val numbers = new int[]{};

        val optionalMinPositive = ArrayUtil.findMinPositive(numbers);

        assertTrue(optionalMinPositive.isEmpty());
    }

    static Stream<Arguments> noPositiveElementsArrays() {
        return Stream.of(
            Arguments.of(new int[] {-1, -8, -10, -15}),
            Arguments.of(new int[] {-5, 0, -6}),
            Arguments.of(new int[] {0, 0, 0}),
            Arguments.of(new int[] {0}),
            Arguments.of(new int[] {-12515})
        );
    }

    @ParameterizedTest
    @MethodSource("noPositiveElementsArrays")
    public void testNoPositiveElements(final int[] numbers) {
        val optionalMinPositive = ArrayUtil.findMinPositive(numbers);

        assertTrue(optionalMinPositive.isEmpty());
    }

    static Stream<Arguments> atLeastTwoPositiveElementsLeftSmallerThanRightSource() {
        return Stream.of(
            Arguments.of(1, new int[] {1, 2}),
            Arguments.of(5, new int[] {5, -6, 10}),
            Arguments.of(5, new int[] {5, 6, 7, 12}),
            Arguments.of(3, new int[] {0, 3, 0, 0, Integer.MAX_VALUE})
        );
    }

    @ParameterizedTest
    @MethodSource("atLeastTwoPositiveElementsLeftSmallerThanRightSource")
    public void testAtLeastTwoPositiveElementsLeftSmallerThanRight(final int expectedMinPositive, final int[] numbers) {
        val optionalMinPositive = ArrayUtil.findMinPositive(numbers);

        optionalMinPositive.ifPresentOrElse(
            actualMinPositive -> assertEquals(expectedMinPositive, actualMinPositive),
            Assertions::fail
        );
    }

    static Stream<Arguments> atLeastOnePositiveElementSource() {
        return Stream.of(
            Arguments.of(1, new int[] {1, 2, 1}),
            Arguments.of(1, new int[] {5, 18, 1, 2}),
            Arguments.of(5, new int[] {0, 0, 0, 5, 0, 0}),
            Arguments.of(1, new int[] {1, 3, 0, 0, 20}),
            Arguments.of(Integer.MAX_VALUE, new int[] {Integer.MAX_VALUE})
        );
    }

    @ParameterizedTest
    @MethodSource("atLeastOnePositiveElementSource")
    public void testAtLeastOnePositiveElement(final int expectedMinPositive, final int[] numbers) {
        val optionalMinPositive = ArrayUtil.findMinPositive(numbers);

        optionalMinPositive.ifPresentOrElse(
            actualMinPositive -> assertEquals(expectedMinPositive, actualMinPositive),
            Assertions::fail
        );
    }
}