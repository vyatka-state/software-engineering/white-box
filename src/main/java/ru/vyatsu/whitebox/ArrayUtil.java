package ru.vyatsu.whitebox;

import java.util.Optional;

public class ArrayUtil {

    private static final long INT_MAX_VALUE_EXCLUSIVE = (long) Integer.MAX_VALUE + 1;

    private ArrayUtil() {
    }

    /**
     * @param numbers массив чисел
     * @return минимальное положительное число из массива numbers
     */
    public static Optional<Integer> findMinPositive(int[] numbers) {
        long minPositive = INT_MAX_VALUE_EXCLUSIVE; // 1
        for (int number : numbers) { // 2
            if (number > 0 /* 3 */ && number < minPositive /* 4*/) {
                minPositive = number; // 5
            }
        }

        Optional<Integer> optionalMinPositive = minPositive == INT_MAX_VALUE_EXCLUSIVE // 6
            ? Optional.empty() // 7
            : Optional.of((int) minPositive); // 8

        return optionalMinPositive; // 9
    }
}
